package com.example.viewpg

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpg.adapter.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager2
    private lateinit var viewPagerTabs: TabLayout

    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)
        viewPager.adapter = viewPagerFragmentAdapter

        TabLayoutMediator(viewPagerTabs, viewPager) { tab, position ->
            when (position) {
                0 -> tab.setIcon(R.drawable.ic_baseline_edit_note_24)
                1 -> tab.setIcon(R.drawable.ic_baseline_image_24)
                2 -> tab.setIcon(R.drawable.ic_baseline_coronavirus_24)
            }
        }.attach()

    }

    private fun init() {
        viewPager = findViewById(R.id.viewPager)
        viewPagerTabs = findViewById(R.id.viewPagerTabs)
    }

}